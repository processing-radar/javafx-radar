package model;

import javafx.scene.paint.Color;
import util.Variables;

public class SceneObj {

	protected int x, y, z, thickness, size;
	protected int[][] pos;
	protected Color rgb;

	// Constructs scene object with z value
	public SceneObj(int x, int y, int z, int thickness, Color rgb) {

		this.x = x;
		this.y = y;
		this.z = (z < 1) ? 1 : z;

		this.thickness = thickness > 1 ? thickness : 1;

		this.rgb = rgb;

		size = 0;
	}

	// Adds the point position in the array of positions
	protected void addPos(int x, int y) {

		// Checks if position has already been added
		for (int i = 0; i < size; i++) {
			if (pos[i][0] == x && pos[i][1] == y)
				return;
		}

		pos[size][0] = ((Variables.FOCUS / z) * x) + Variables.CENTER_X;
		pos[size][1] = ((Variables.FOCUS / z) * y) + Variables.CENTER_Y;
		size++;
	}

	// Return the x value
	public int getX() {
		return x;
	}

	// Return the y value
	public int getY() {
		return y;
	}

	// Returns array of points positions
	public int[][] getPos() {
		return pos;
	}

	// Return the object color
	public Color getColor() {
		return rgb;
	}

}
