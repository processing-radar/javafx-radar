package model;

import javafx.scene.paint.Color;

public class Point extends SceneObj {

	public Point(int x, int y, int z, int thickness, Color rgb) {
		super(x, y, z, thickness, rgb);

		pos = new int[5][2];
		
		genPos();
	}

	// Generates the position of the pixel
	private void genPos() {
		
		addPos(x, y);
		
		switch (thickness) {			
		case 2: 
			addPos(x, y);
			addPos(x + 1, y);
			addPos(x, y + 1);
			addPos(x + 1, y + 1);
			break;
			
		case 3:
			addPos(x + 1, y);
			addPos(x, y + 1);
			addPos(x - 1, y);
			addPos(x, y - 1);
			break;
		}
	}
	
	// Updates the position of the point
	public void updatePos() {

	}
	
//	private void genPos(int loadX, int loadY, int curX, int curY) {
//
//		addPos(curX, curY + 1); // Add the current positions
//		addPos(curX + 1, curY);
//		
//		if (loadX > 2 && loadY > 2) {
//			genPos(loadX, loadY - 1, curX, curY + 1);
//			genPos(loadX - 1, loadY, curX + 1, curY);
//		}
//
//		addPos(curX, curY - 1); // Mirrors the current positions
//		addPos(curX - 1, curY);
//		if (loadX > 2) {
//			genPos(loadX - 1, loadY - 1, curX, curY - 1);
//			genPos(loadX - 1, loadY, curX - 1, curY);
//		}
//	}

}
