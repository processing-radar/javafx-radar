package model;

import javafx.scene.paint.Color;
import util.Variables;

public class Circle extends SceneObj {

	private int curX, curY, d;

	public Circle(int centerX, int centerY, int z, int radius, int thickness, Color rgb) {
		super(centerX, centerY, z, thickness, rgb);

		curX = 0; // Current position
		curY = (Variables.FOCUS / z) * radius;

		d = 5 - curY; // Constant

		// Two dimensional array of positions
		pos = new int[curY * 8][2];
		
		genPos();
	}

	// Generates the position of the point
	private void genPos() {

		while (curX <= curY) {
			addPos();
			if (d < 0)
				d += 2 * curX + 1;
			else {
				d += 2 * (curX - curY) + 1;
				curY--;
			}
			curX++;
		}
	}

	// Adds the position of the point
	private void addPos() {

		// Loop mirrors the position of the point for all circle octets
		for (int i = 0; i < 4; i++) {

			// Positions
			addPos(x + curX, y + curY);

			// Mirrored
			addPos(x + curY, y + curX);

			if (i == 0 || i == 2)
				curX *= -1;
			else
				curY *= -1;
		}
	}

	// Update the position of the point in the screen
	public void updatePos() {

	}

}
