package model;

import javafx.scene.paint.Color;
import util.Variables;

public class Line extends SceneObj {

	private int x2, y2;

	private int d, dx, dy, dx2, dy2, ix, iy;

	public Line(int x, int y, int z, int x2, int y2, int thickness, Color rgb) {
		super(x, y, z, thickness, rgb);

		this.x2 = x2 + Variables.CENTER_X;
		this.y2 = y2 + Variables.CENTER_Y;

		genPos();
	}

	// Prepares the variables to generate the points positions
	private void genPos() {

		// Delta of exact value and rounded value of the dependent variable
		d = 0;

		dx = Math.abs(x2 - x); // Delta X
		dy = Math.abs(y2 - y); // Delta Y

		dx2 = 2 * dx; // Slope scaling factors to
		dy2 = 2 * dy; // avoid floating point

		ix = x < x2 ? 1 : -1; // Increment direction
		iy = y < y2 ? 1 : -1;

		// Two dimensional array of positions
		pos = (dx >= dy) ? new int[dx][2] : new int[dy][2];

		if (dx >= dy)
			otherDegrees();
		else
			ninetyDegrees();
	}

	// Adds the positions of the points if the line is through
	// first, fourth, fifth and eighth octets
	private void otherDegrees() {

		while (x != x2) {
			addPos(x, y);
			x += ix;
			d += dy2;
			if (d > dx) {
				y += iy;
				d -= dx2;
			}
		}
	}

	// Adds the positions of the points if the line is through
	// second, third, sixth and seventh octets
	private void ninetyDegrees() {

		while (y != y2) {
			addPos(x, y);
			y += iy;
			d += dx2;
			if (d > dy) {
				x += ix;
				d -= dy2;
			}
		}
	}

	// Update the position of the point in the screen
	public void updatePos() {

	}

}
