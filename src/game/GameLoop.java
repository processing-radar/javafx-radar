package game;

import java.util.ArrayList;

import javafx.animation.AnimationTimer;
import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;
import model.Circle;
import model.Line;
import model.Point;

public class GameLoop {

	// Manipulates pixel
	private final PixelWriter pw;

	// Scene objects lists
	private ArrayList<Point> points = new ArrayList<Point>();
	private ArrayList<Line> lines = new ArrayList<Line>();
	private ArrayList<Circle> circles = new ArrayList<Circle>();

	public GameLoop(PixelWriter pw) {

		// Class to draw pixel in screen
		this.pw = pw;

		// Creates and adds objects to respective arrays
		circles.add(new Circle(0, 0, 1, 10, 1, Color.RED));
		circles.add(new Circle(0, 0, 2, 10, 1, Color.WHITE));
		//lines.add(new Line(-180, -180, 0, 0, 0, 20, Color.RED));
		//points.add(new Point(200, 200, 0, 2, Color.RED));

		// Start animation loop
		startLoop();
	}

	// Updates the objects in the scene
	private void update() {

		// Updates points positions
		for (Point p : points) {
			p.updatePos();
		}

		// Updates lines positions
		for (Line l : lines) {
			l.updatePos();
		}

		// Updates circles positions
		for (Circle c : circles) {
			c.updatePos();
		}
	}

	// Repaints the canvas
	private void refresh() {

		// Paints the received points at the screen
		for (Point p : points) {
			int[][] arr = p.getPos();
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] != null) {
					pw.setColor(arr[i][0], arr[i][1], p.getColor());
				}
			}
		}

		// Paints the received lines at the screen
		for (Line l : lines) {
			int[][] arr = l.getPos();
			for (int i = 0; i < arr.length; i++) {
				pw.setColor(arr[i][0], arr[i][1], l.getColor());
			}
		}

		// Paints the received circles at the screen
		for (Circle c : circles) {
			int[][] arr = c.getPos();
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] != null) {
					pw.setColor(arr[i][0], arr[i][1], c.getColor());
				}
			}
		}
	}

	// Starts the main loop
	private void startLoop() {
		new AnimationTimer() {
			public void handle(long curNanoTimer) {
				update();
				refresh();
			}
		}.start();
	}
}
