package application;

import game.GameLoop;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import util.Variables;

public class Main extends Application {

	// Start the stage
	@Override
	public void start(Stage stage) {

		try {
			// Sets the title of the stage
			stage.setTitle("Radar");

			// Creates scene
			final Group root = new Group();
			final Scene scene = new Scene(root, Variables.WIDTH, Variables.HEIGHT, Variables.BACK_COLOR);
			stage.setScene(scene);

			// Creates a canvas
			Canvas canvas = new Canvas(Variables.WIDTH, Variables.HEIGHT);
			root.getChildren().add(canvas);
			
			// Create the main loop of the program
			new GameLoop(canvas.getGraphicsContext2D().getPixelWriter());

			// Show the window
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Main method
	public static void main(String[] args) {
		launch(args);
	}
}
