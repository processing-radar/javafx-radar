package util;

import javafx.scene.paint.Color;

public class Variables {

	// Window size
	public static final int WIDTH = 1920;
	public static final int HEIGHT = 1080;

	// Center window
	public static final int CENTER_X = WIDTH / 2;
	public static final int CENTER_Y = HEIGHT / 2;

	// Background Color
	public static final Color BACK_COLOR = Color.rgb(0, 0, 0);

	// Observer
	public static final int[][] OBSERVER = { { 0, 0, 0 } };
	
	//
	public static final int FOCUS = 10;
}
